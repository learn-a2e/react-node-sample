const express=require('express');
const routes=express.Router();
const bookroute=require('./Books/controllers/book.controller');
const authorroute=require('./Books/controllers/author.controller');

routes.use('/books',bookroute);
routes.use('/authors',authorroute);

module.exports=routes;