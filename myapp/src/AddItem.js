import React, { Component } from 'react';
import './App.css';

class AddItem extends Component {

    constructor(props){
        super(props);
        this.onSubmit=this.onSubmit.bind(this);
    }

    onSubmit(event){
        event.preventDefault();
        this.props.onAdd(
            this.nameInput.value,
            this.ISBNInput.value,
            this.AuthorInput.value,
            this.PriceInput.value,
            this.YearInput.value,
            this.PublisherInput.value
        );

        this.nameInput.value='';
        this.ISBNInput.value='';
        this.AuthorInput.value='';
        this.PriceInput.value='';
        this.YearInput.value='';
        this.PublisherInput.value='';
    }

    render() {
        const {authors}=this.props;

        return(
            <form onSubmit={this.onSubmit} className="App-header">
                <h3 >Add product</h3>
                <div>
                    <input placeholder="Name" ref={nameInput =>this.nameInput=nameInput} />
                </div>
                <div>
                    <input placeholder="ISBN" ref={ISBNInput =>this.ISBNInput=ISBNInput}/>
                </div>
                <div>
                    <select ref={AuthorInput =>this.AuthorInput=AuthorInput}>
                        <option selected disabled>--Author Name--</option>
                        {
                            authors.map(author=><option key={author._id}>{author.fname}</option>)
                        }
                    </select>
                </div>
                <div>
                    <input placeholder="Price" ref={PriceInput =>this.PriceInput=PriceInput}/>
                </div>
                <div>
                    <input placeholder="Year" ref={YearInput =>this.YearInput=YearInput} />
                </div>
                <div>
                    <input placeholder="Publisher" ref={PublisherInput =>this.PublisherInput=PublisherInput}/>
                </div>

                <button>Add</button>
                <hr/>
                <hr/>
            </form>
        );
    }
}

export default AddItem;
